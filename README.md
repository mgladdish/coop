# coop

A tool to parse and fix bugs in [Co-Op Businesses bank account](https://www.co-operativebank.co.uk/business/online-banking) 
statement exports, and translate them into a format that can be uploaded to [FreeAgent](https://freeagent.com).

## CoOp Bugs

There's at least one bug in CoOp's Business account statement download, in that at least one transaction gets smudged 
across both the Withdrawals and Deposits column.

Here's a snippet from a recent statement download. The actual transaction was a Withdrawal of £11.28 but here it's shown 
as a Withdrawal of £11 and a Deposit of £28. 

```
             Date           Description                                                                 Withdrawals                 Deposits                  Balance                              
                                                                                                                                                                                                   
                                                                                                                                                                                                   
                                                                                                                                                                                                   
           17 APR 20        BROUGHT FORWARD                                                                                                                158,029.62                              
                                                                                                                                                                                                   
                                                                                                                                                                                                   
           23 APR 20        ******redacted******                                                                 11                       28               158,018.34                              
```

This program will find rows where there are values in both the Withdrawals and Deposits columns and fix up their values 
accordingly.

## Caveats

This is pretty much my first Go project so be gentle.

Given I no longer trust CoOp's statement downloads I strongly recommend cross-checking anything you upload into FreeAgent
against the running account balance just to make sure everything still lines up.