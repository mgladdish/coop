package row

import (
	"time"
)

// Capture enough information to send to FreeAgent
// https://support.freeagent.com/hc/en-gb/articles/115001222564-CSV-file-formats-for-bank-uploads
type Row struct {
	Date          time.Time
	AmountInPence int
	Description   string
}
