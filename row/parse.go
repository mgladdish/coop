package row

import (
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"
)

func ParseRow(line string) (Row, error) {
	if len(line) < 81 {
		return Row{}, fmt.Errorf("line too short, has %d chars.", len(line))
	}
	log.Printf("date:       [%s]", line[1:10])
	log.Printf("withdrawal: [%s]", line[58:69])
	log.Printf("deposit:    [%s]", line[69:83])

	date, dateErr := parseDate(strings.TrimSpace(line[1:10]))
	if dateErr != nil {
		return Row{}, dateErr
	}

	description := strings.TrimSpace(line[11:57])
	withdrawal, withdrawalErr := parseAmount(strings.TrimSpace(line[58:69]))
	deposit, depositErr := parseAmount(strings.TrimSpace(line[69:83]))

	if withdrawalErr != nil && depositErr != nil {
		return Row{}, withdrawalErr
	}

	if deposit != 0 && withdrawal != 0 {
		withdrawal = (withdrawal * 100) + deposit
		deposit = 0
	}

	return Row{
		Date:          date,
		AmountInPence: deposit - withdrawal,
		Description:   description,
	}, nil

}

func parseDate(possibleDate string) (time.Time, error) {
	layout := "2 Jan 06"
	return time.Parse(layout, possibleDate)
}

func parseAmount(possibleAmount string) (int, error) {
	withoutCommas := strings.ReplaceAll(possibleAmount, ",", "")
	withoutPeriods := strings.ReplaceAll(withoutCommas, ".", "")
	return strconv.Atoi(withoutPeriods)
}
