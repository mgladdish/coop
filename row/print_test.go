package row

import (
	"bytes"
	"testing"
	"time"
)

func TestFormatAmountPence(t *testing.T) {
	actual := formatAmount(23)
	assertStringsMatch(t, "0.23", actual)
}

func TestFormatAmountPoundsAndPence(t *testing.T) {
	assertStringsMatch(t, "108.62", formatAmount(10862))
}

func TestFormatAmountThousandsPounds(t *testing.T) {
	assertStringsMatch(t, "-3821.00", formatAmount(-382100))
}

func TestFormatDate(t *testing.T) {
	assertStringsMatch(t, "09/12/2020", formatDate(date(2020, time.December, 9)))
}

func TestPrintRow(t *testing.T) {
	row := Row{
		date(2019, time.September, 11),
		10862,
		"Invoice 24",
	}

	var b bytes.Buffer
	err := PrintRow(row, &b)
	expectNoError(t, err)

	result := b.String()
	assertStringsMatch(t, "11/09/2019,108.62,Invoice 24\n", result)
}

func assertStringsMatch(t *testing.T, expected string, actual string) {
	if expected != actual {
		t.Errorf("Expected [%s] but got [%s]", expected, actual)
	}
}
