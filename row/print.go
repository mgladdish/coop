package row

import (
	"encoding/csv"
	"fmt"
	"io"
	"time"
)

func PrintRow(row Row, writer io.Writer) error {
	csvWriter := csv.NewWriter(writer)

	columns := []string{
		formatDate(row.Date),
		formatAmount(row.AmountInPence),
		row.Description,
	}

	err := csvWriter.Write(columns)

	csvWriter.Flush()

	return err
}

func formatDate(date time.Time) string {
	format := "02/01/2006"
	return date.Format(format)
}

func formatAmount(amountInPence int) string {
	return fmt.Sprintf("%.2f", float32(amountInPence)/100)
}
