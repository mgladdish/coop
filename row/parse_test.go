package row

import (
	"testing"
	"time"
)

func TestParseDateWithValidDate(t *testing.T) {
	date, dateErr := parseDate("17 APR 20")
	expectNoError(t, dateErr)

	if date.Day() != 17 && date.Month() != 4 && date.Year() != 2020 {
		t.Errorf("Expected 17th April 2020 but got %d %d %d instead", date.Day(), date.Month(), date.Year())
	}
}

func TestParseDateWithInvalidDate(t *testing.T) {
	_, dateErr := parseDate("   ")
	if dateErr == nil {
		t.Errorf("Was expecting an error but got this: %s", dateErr)
	}
}

func TestParseAmountWithPence(t *testing.T) {
	amount, amountErr := parseAmount("11")
	expectNoError(t, amountErr)

	if amount != 11 {
		t.Errorf("Was expecting 11 but got %d", amount)
	}
}

func TestParseAmountWithPoundsAndPence(t *testing.T) {
	pence, penceErr := parseAmount("0.98")
	expectNoError(t, penceErr)

	if pence != 98 {
		t.Errorf("Expected 98 but got %d", pence)
	}

	pounds, poundsErr := parseAmount("35.78")
	expectNoError(t, poundsErr)

	if pounds != 3578 {
		t.Errorf("Expected 3578 but got %d", pounds)
	}
}

func TestParseAmountWithThousandsPoundsAndPence(t *testing.T) {
	thousands, thousandsErr := parseAmount("8415.29")
	expectNoError(t, thousandsErr)

	if thousands != 841529 {
		t.Errorf("Expected 841529 but got %d", thousands)
	}
}

func TestParseWithProblemRow(t *testing.T) {
	parsed, err := ParseRow("           23 APR 20        8036 MSFT *&#60E0100A                                                                11                       28               158,018.34")
	expectNoError(t, err)

	if parsed.Date != date(2020, time.April, 23) {
		t.Errorf("Expected 23rd April 2020 but got %s", parsed.Date)
	}

	expectedDescription := "8036 MSFT *&#60E0100A"
	if parsed.Description != expectedDescription {
		t.Errorf("Expected %s but got %s", expectedDescription, parsed.Description)
	}

	expectedAmount := -1128
	if parsed.AmountInPence != expectedAmount {
		t.Errorf("Expected %d but got %d", expectedAmount, parsed.AmountInPence)
	}
}

var statement = []struct {
	line string
	row  Row
}{
	{"           17 APR 20        BROUGHT FORWARD                                                                                                                158,029.62",
		Row{}},
	{"           23 APR 20        8036 MSFT *&#60E0100A                                                                11                       28               158,018.34",
		Row{date(2020, time.April, 23), -1128, "8036 MSFT *&#60E0100A"}},
	{"           27 APR 20        8036 CCC GBP 35.78                                                                 0.98                                                  ",
		Row{date(2020, time.April, 27), -98, "8036 CCC GBP 35.78"}},
	{"           27 APR 20        8036 NUTSHELL                                                                     35.78                                                  ",
		Row{date(2020, time.April, 27), -3578, "8036 NUTSHELL"}},
	{"           27 APR 20        HMRC 9765909168A00108A                                                         8,415.29                                        149,566.29",
		Row{date(2020, time.April, 27), -841529, "HMRC 9765909168A00108A"}},
	{"           28 APR 20        EEUKLTD CA EEUK BATCH 794                                                                               7,200.00               156,766.29",
		Row{date(2020, time.April, 28), 720000, "EEUKLTD CA EEUK BATCH 794"}},
	{"           01 MAY 20        DD HISCOX DD 0547634/0019760439                                                   33.72                                        156,732.57",
		Row{date(2020, time.May, 1), -3372, "DD HISCOX DD 0547634/0019760439"}},
	{"           04 MAY 20        8036 AWS EMEA                                                                      0.98                                                  ",
		Row{date(2020, time.May, 4), -98, "8036 AWS EMEA"}},
	{"           04 MAY 20        8036 GOOGLE *GSUIT                                                                 4.15                                                  ",
		Row{date(2020, time.May, 4), -415, "8036 GOOGLE *GSUIT"}},
	{"           04 MAY 20        8036 AWS EMEA                                                                    563.97                                                  ",
		Row{date(2020, time.May, 4), -56397, "8036 AWS EMEA"}},
	{"           04 MAY 20        DD EE LIMITED Q83521883117751033                                                  24.11                                        156,139.36",
		Row{date(2020, time.May, 4), -2411, "DD EE LIMITED Q83521883117751033"}},
	{"           05 MAY 20        COMMISSION                                                                         1.75                                                  ",
		Row{date(2020, time.May, 5), -175, "COMMISSION"}},
	{"           05 MAY 20        SERVICE CHARGE                                                                     7.00                                                  ",
		Row{date(2020, time.May, 5), -700, "SERVICE CHARGE"}},
	{"           05 MAY 20        FEE DELTA CARD-PURCHASES                                                           2.80                                        156,127.81",
		Row{date(2020, time.May, 5), -280, "FEE DELTA CARD-PURCHASES"}},
	{"           11 MAY 20        DD OLD MUTUAL LIFE 400098625-3                                                 1,000.00                                        155,127.81",
		Row{date(2020, time.May, 11), -100000, "DD OLD MUTUAL LIFE 400098625-3"}},
	{"           14 MAY 20        8036 CCC GBP 17.20                                                                 0.47                                                  ",
		Row{date(2020, time.May, 14), -47, "8036 CCC GBP 17.20"}},
	{"           14 MAY 20        8036 ATLASSIAN                                                                    17.20                                        155,110.14",
		Row{date(2020, time.May, 14), -1720, "8036 ATLASSIAN"}},
}

func TestParseEntireStatement(t *testing.T) {
	for _, entry := range statement {
		parsed, err := ParseRow(entry.line)
		if (entry.row == Row{} && err == nil) {
			t.Errorf("Expected an error but did not get one for %s", entry.line)
		} else if parsed != entry.row {
			t.Errorf("Expected %+v but got %+v", entry.row, parsed)
		}
	}
}

func expectNoError(t *testing.T, err error) {
	if err != nil {
		t.Errorf("Was not expecting an error but got this: %s", err)
	}
}

func date(year int, month time.Month, day int) time.Time {
	return time.Date(year, month, day, 0, 0, 0, 0, time.UTC)
}
