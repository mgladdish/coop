package main

import (
	"bufio"
	"fmt"
	"log"
	"odum.co.uk/coop/convertToFreeAgent/row"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	if len(os.Args) != 3 {
		fmt.Println("Usage:", os.Args[0], "inputfilename", "outputfilename")
		return
	}
	coopStatement, coopStatementErr := os.Open(os.Args[1])
	check(coopStatementErr)
	defer coopStatement.Close()

	output, outputErr := os.Create(os.Args[2])
	check(outputErr)
	defer output.Close()

	scanner := bufio.NewScanner(coopStatement)
	for scanner.Scan() {
		line := scanner.Text()
		parsedRow, rowErr := row.ParseRow(line)
		if rowErr == nil {
			printErr := row.PrintRow(parsedRow, output)
			if printErr != nil {
				log.Panicf("Error printing line %s, %s", line, printErr)
			}
		} else {
			log.Printf("Ignoring line %s", line)
		}
	}
}
